/*
An attempt to implement a binary search tree
Rapando
Oct 11th, 2021
*/

package main

import (
	"fmt"

	"gitlab.com/go-binary-tree/tree"
)

func main() {
	var t = tree.Tree{}
	t.Insert('F')
	t.Insert('B')
	t.Insert('A')
	t.Insert('D')
	t.Insert('C')
	t.Insert('E')
	t.Insert('G')
	t.Insert('I')
	t.Insert('H')

	fmt.Println("Pre Order  : ")
	tree.PrintPreOrder(t.Root)
	fmt.Println("\nPost Order : ")
	tree.PrintPostOrder(t.Root)
	fmt.Println("\nIn Order   :")
	tree.PrintInOrder(t.Root)
	fmt.Println("\n=============")
}
