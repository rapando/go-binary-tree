package tree

import "fmt"

func PrintPreOrder(n *Node) {
	if n == nil {
		return
	}
	fmt.Printf("%c ", n.Key)
	PrintPreOrder(n.Left)
	PrintPreOrder(n.Right)
}

func PrintPostOrder(n *Node) {
	if n == nil {
		return
	}
	PrintPostOrder(n.Left)
	PrintPostOrder(n.Right)
	fmt.Printf("%c ", n.Key)
}

func PrintInOrder(n *Node) {
	if n == nil {
		return
	}
	PrintInOrder(n.Left)
	fmt.Printf("%c ", n.Key)
	PrintInOrder(n.Right)
}
