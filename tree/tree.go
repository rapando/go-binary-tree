package tree

type Node struct {
	Key   byte
	Left  *Node
	Right *Node
}
type Tree struct {
	Root *Node
}

// insert to tree
func (t *Tree) Insert(data byte) {
	if t.Root == nil {
		t.Root = &Node{Key: data}
	} else {
		t.Root.Insert(data)
	}
}

// insert into node
func (n *Node) Insert(data byte) {
	if data <= n.Key {
		if n.Left == nil {
			n.Left = &Node{Key: data}
		} else {
			n.Left.Insert(data)
		}
	} else {
		if n.Right == nil {
			n.Right = &Node{Key: data}
		} else {
			n.Right.Insert(data)
		}
	}
}
